<?php
    if ( ! defined("BASEPATH")) exit("No direct script access allowed");
    class Akun_model extends CI_Model{
        var $CI = NULL;
        var $table=array('admin'=>'user','guest'=>'user_guest');
        var $valid_session=array(1,'HOUR');

        var $rules=array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email'
            ),
            array(
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ),
            array(
                'field' => 'nomor_hp',
                'label' => 'Nomor HP',
                'rules' => 'required'
            )
        );
        public function __construct() {
            $this->CI =& get_instance();

            // $query=array(
            //     'expired'=>1
            // );
            // $this->destroySession($query);
        }

        function get($role,$data=NULL){
            if(isset($data)){
                if(isset($data['select'])){
                    if(is_array($data['select'])){
                        foreach($data['select'] as $select){
                            $this->db->select($select);
                        }
                    }
                }

                if(isset($data['param'])){
                    if(is_array($data['param'])){
                        $allowed=array('username','password');
                        foreach($allowed as $param){
                            if(isset($data['param'][$param])){
                                $this->db->where($param,$this->db->escape_str($data['param'][$param]));
                            }
                        }
                    }
                }

                if(isset($data['limit'])){
                    if(is_array($data['limit'])){
                        if(sizeof($data['limit'])==2){
                            $this->db->limit($data['limit'][0]>0 ? $data['limit'][0] : 1, $data['limit'][1]>0 ? $data['limit'][1] : 1);
                        }
                        else{
                            $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                        }
                    }
                    else{
                        $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                    }
                }
            }

            $query=$this->db->get($this->table[$role]);

            return $query;
        }

        function tambah($role, &$data){
            $data['password']=md5($data['password']);
            if(in_array($role,array_keys($this->table))){
                $this->db->trans_begin();
    
                if($this->db->insert($this->table[$role],$data)){
                    unset($data['password']);
                    if($this->db->trans_status()===TRUE){
                        $data=array_merge(array('id'=>$this->db->insert_id()),$data);
                        $this->db->trans_commit();
                        return true;
                    }
                    else{
                        $this->db->trans_rollback();
                        return false;
                    }
                }
                else{
                    $this->db->trans_rollback();
                    return false;
                }
            }
            else{
            }
        }

        function edit($role, &$data){
            if(in_array($role,array_keys($this->table))){
                $this->db->trans_begin();

                foreach(array_keys($data) as $key){
                    if(strtolower($key)!='password'){
                        $data[$key]=$this->db->escape_str($data[$key]);
                    }
                    else{
                        $data[$key]=md5($data[$key]);
                    }
                }

                if(isset($data['param'])){
                    if(is_array($data['param'])){
                        $allowed=array('id','username','email','nomor_hp');
                        foreach($allowed as $param){
                            if(isset($data['param'][$param])){
                                $this->db->where($param,$this->db->escape_str($data['param'][$param]));
                            }
                        }
                    }
                }

                if(isset($data['limit'])){
                    if(is_array($data['limit'])){
                        if(sizeof($data['limit'])==2){
                            $this->db->limit($data['limit'][0]>0 ? $data['limit'][0] : 1, $data['limit'][1]>0 ? $data['limit'][1] : 1);
                        }
                        else{
                            $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                        }
                    }
                    else{
                        $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                    }
                }

                if($this->db->update($this->table[$role],$data)){
                    unset($data['password']);
                    if($this->db->trans_status()===TRUE){
                        $data=array_merge(array('id'=>$this->db->insert_id()),$data);
                        $this->db->trans_commit();
                        return true;
                    }
                    else{
                        $this->db->trans_rollback();
                        return false;
                    }
                }
                else{
                    $this->db->trans_rollback();
                    return false;
                }
            }
            else{
            }
        }

        function delete($role, $data){
            if(in_array($role, array_keys($this->table))){
                $this->db->trans_begin();

                if(isset($data['param'])){
                    if(is_array($data['param'])){
                        $allowed=array('id','username','email','nomor_hp');
                        foreach($allowed as $param){
                            if(isset($data['param'][$param])){
                                $this->db->where($param,$this->db->escape_str($data['param'][$param]));
                            }
                        }
                    }
                }

                if(isset($data['limit'])){
                    if(is_array($data['limit'])){
                        if(sizeof($data['limit'])==2){
                            $this->db->limit($data['limit'][0]>0 ? $data['limit'][0] : 1, $data['limit'][1]>0 ? $data['limit'][1] : 1);
                        }
                        else{
                            $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                        }
                    }
                    else{
                        $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                    }
                }

                if($this->db->delete($this->table[$role])){
                    if($this->db->trans_status()===true){
                        if($this->db->affected_rows()>0){
                            $this->db->trans_commit();
                            return true;
                        }
                        else{
                            $this->db->trans_rollback();
                            return false;
                        }
                    }
                    else{
                        $this->db->trans_rollback();
                        return false;
                    }
                }
                else{
                    $this->db->trans_rollback();
                    return false;
                }
            }
            else{
                return false;
            }
        }
    }
?>