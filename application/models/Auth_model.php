<?php
    if ( ! defined("BASEPATH")) exit("No direct script access allowed");
    class Auth_model extends CI_Model{
        var $CI = NULL;
        var $table=array('admin'=>'user','guest'=>'user_guest');

        var $rules=array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            )
        );
        public function __construct() {
            $this->CI =& get_instance();
        }

        function login($role,&$data=NULL){
            if(in_array($role,array_keys($this->table))){
                if(isset($data)){
                    $rules=array(
                        array(
                            'field' => 'username',
                            'label' => 'Username',
                            'rules' => 'required'
                        ),
                        array(
                            'field' => 'password',
                            'label' => 'Password',
                            'rules' => 'required'
                        )
                    );
                    $this->form_validation->set_rules($rules);
                    $this->form_validation->set_data($data);
    
                    if($this->form_validation->run()==true){
                        $data=array(
                            'param'=>array(
                                'username'=>$data['username'],
                                'password'=>md5($data['password'])
                            ),
                            'limit'=>1
                        );
    
                        if($data=$this->akun_model->get($role,$data)){
                            if($data->num_rows()){
                                $data=$data->result_array()[0];

                                $this->db->trans_begin();

                                $data=array(
                                    'id_'.$role=>$data['id'],
                                    'role'=>$role,
                                    'ip_address'=>$_SERVER['REMOTE_ADDR'],
                                    'session_timestamp'=>date("Y-m-d H:i:s")
                                );
                                $data=array_merge(
                                    array(
                                        'token'=>md5($_SERVER['REMOTE_ADDR'].'_'.$role.'_'.$data['id_'.$role].'_'.$data['session_timestamp'])
                                    ),
                                    $data
                                );
                                if($this->db->insert('session_token',$data)){
                                    if($this->db->trans_status()===true){
                                        $this->db->trans_commit();
                                        return true;
                                    }
                                    else{
                                        $this->db->trans_rollback();
                                        return false;
                                    }
                                }
                                else{
                                    $this->db->trans_rollback();
                                    return false;
                                }
                            }
                            else{
                                return false;
                            }
                        }
                        else{
                            return false;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
    }
?>