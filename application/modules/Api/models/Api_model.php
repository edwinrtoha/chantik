<?php
    if ( ! defined("BASEPATH")) exit("No direct script access allowed");
    class Api_model extends CI_Model{
        var $CI = NULL;
        public function __construct() {
            $this->CI =& get_instance();
        }

        function get_allow_origin(){
            header('Access-Control-Allow-Origin: *');
        }

        function response($response){
            if(is_array($response)){
                if(sizeof($response)==2){
                    $respon=json_encode($response[0],JSON_PRETTY_PRINT);
                    http_response_code($response[1]);
                }
                else{
                    $respon=json_encode($response[0],JSON_PRETTY_PRINT);
                    http_response_code(200);
                }
            }
            else{
                $respon=json_encode(array('error'));
                http_response_code(204);
            }
            return $respon;
        }
        
        function getBearerToken() {
            $headers=$this->input->request_headers();
            if(isset($headers['Authorization'])){
                if(preg_match('/Bearer\s(\S+)/', $headers['Authorization'], $matches)){
                    if(isset($matches[1])){
                        return $matches[1];
                    }
                    else{
                        return null;
                    }
                }
                else{
                    return null;
                }
            }
            else{
                return null;
            }
        }
        
        function token_validation($role=NULL){
            $token=$this->api_model->getBearerToken();
            $response['token']=$token;
            
            $this->db->where('token',$this->db->escape_str($token));
            if(isset($role)){
                $this->db->where('role',$this->db->escape_str($role));
            }
            $this->db->limit(1);
            if($query=$this->db->get('session_token')){
                if($query->num_rows()){
                    $response['status']='valid';
                }
                else{
                    $response['status']='invalid';
                }
            }
            else{
                $response['status']='invalid';
            }
            return $response;
        }
    }
?>