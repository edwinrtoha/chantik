<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    var $CI = NULL;
	public function __Construct(){
		$this->CI =& get_instance();
        header('Content-type: application/json');
        parent ::__construct();
        $this->load->model('api_model');
        $this->load->model('auth_model');
        $this->load->model('akun_model');
    }

	public function index()
	{
    }
    
    public function login($role=NULL){
        if(isset($role)){
            if(in_array($role,array_keys($this->akun_model->table))){
                switch(strtolower($this->input->method())){
                    case 'post':
                        $data=$this->input->post();
                        if($this->auth_model->login($role,$data)==true){
                            echo $this->api_model->response(array($data));
                        }
                        else{
                            echo $this->api_model->response(array(array()));
                        }
                        break;
                    default:
                        http_response_code(405);
                        break;
                }
            }
            else{
                show_404();
            }
        }
        else{
            show_404();
        }
    }
}