<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {

    var $CI = NULL;
	public function __Construct(){
		$this->CI =& get_instance();
        header('Content-type: application/json');
        parent ::__construct();
        $this->load->model('api_model');
        $this->load->model('akun_model');
    }

	public function index()
	{
    }
    
    public function admin($sub=NULL){
        $role='admin';
        if(isset($sub)){
            switch(strtolower($sub)){
                case 'delete':
                    switch($this->input->method()){
                        case 'post':
                            $token_validation=$this->api_model->token_validation($role);
                            if($token_validation['status']=='valid'){
                                $data=array(
                                    'param'=>array(
                                        'id'=>$this->input->post('id')
                                    ),
                                    'limit'=>1
                                );
                                if($this->akun_model->delete($role,$data)){
                                    echo $this->api_model->response(array(array('api_status'=>'success')));
                                }
                                else{
                                    echo $this->api_model->response(array(array('error'=>'Internal Server Error'),500));
                                }
                            }
                            else{
                                echo $this->api_model->response(array(array('error'=>'Invalid Token')));
                            }
                            break;
                        default:
                            show_404();
                            break;
                    }
                    break;
                default:
                    show_404();
                    break;
            }
        }
        else{
            switch(strtolower($this->input->method())){
                case 'get':
                    $data=$this->input->get();
                    if($query=$this->akun_model->get($role,$data)){
                        if($query->num_rows()){
                            $response=$query->result_array();
                            for ($i=0; $i < sizeof($response); $i++) {
                                unset($response[$i]['password']);
                            }
                            $response=array($response);
                        }
                        else{
                            $response=array(array());
                        }
                        echo $this->api_model->response($response);
                    }
                    else{
                        echo $this->api_model->response(array(array('error'=>'Internal Server Error'),500));
                    }
        
                    break;
                case 'post':
                    $token_validation=$this->api_model->token_validation($role);
                    if($token_validation['status']=='valid'){
                        $data=$this->input->post();
                        $this->form_validation->set_data($data);
                        $this->form_validation->set_rules($this->akun_model->rules);
        
                        if($this->form_validation->run()==true){
                            if($this->akun_model->tambah($role,$data)===true){
                                echo $this->api_model->response(array($data));
                            }
                            else{
                                echo $this->api_model->response(array(array('error'=>'Internal Server Error'),500));
                            }
                        }
                        else{
                            foreach($this->akun_model->rules as $rules){
                                if(!empty(form_error($rules['field'],NULL,NULL))){
                                    $response['error'][$rules['field']]=form_error($rules['field'],NULL,NULL);
                                }
                            }
                            echo $this->api_model->response(array($response));
                        }
                    }
                    else{
                        echo $this->api_model->response(array(array('error'=>'Invalid Token')));
                    }
                    
                    break;
                default:
                    http_response_code(405);
                    break;
            }
        }
    }

    public function guest(){
        $role='guest';
        switch(strtolower($this->input->method())){
            case 'get':
                $data=$this->input->get();
                if($query=$this->akun_model->get($role,$data)){
                    if($query->num_rows()){
                        $response=$query->result_array();
                        for ($i=0; $i < sizeof($response); $i++) {
                            unset($response[$i]['password']);
                        }
                        $response=array($response);
                    }
                    else{
                        $response=array(array());
                    }
                    echo $this->api_model->response($response);
                }
                else{
                    echo $this->api_model->response(array(array('error'=>'Internal Server Error'),500));
                }
    
                break;
            case 'post':
                break;
            default:
                http_response_code(405);
                break;
        }
    }
}